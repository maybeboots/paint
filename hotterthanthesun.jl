using CairoMakie
using CSV, DataFrames
using FileIO
using Statistics: mean

ds = CSV.read("GLB.Ts_dSST.csv", DataFrame)
ys = [mean(ds[x, 2:13]) for x in 1:size(ds, 1)]
xs = ds[:, 1]

fin_de_siecle = mean(ys[1:20])
ys .-= fin_de_siecle

f = Figure(resolution = (957, 604), figure_padding=10)

ax = Axis(f[1, 1], xautolimitmargin=(0.02, 0.06), xtickwidth=0, ytickwidth=0, ygridcolor="#f0f0f0", xgridcolor="#f0f0f0", ylabelcolor="#d0d0d0", xtickcolor="#d0d0d0")
ax.xticks = xs[1]:10:xs[end]
ax.xticklabelfont = "Noto"
ax.xticklabelcolor = "#999999"
ax.xticklabelsize = 13

ax.yticks = LinearTicks(10)
ax.ytickformat = xs -> begin
    formatter(x) = ifelse(x > 0, "+", "") * repr(round(x, digits=1)) * "\u00b0"

    xs = formatter.(xs)
    xs[end] *= "C"
    xs
end

ax.yticklabelfont = "Noto"
ax.yticklabelcolor = "#999999"
ax.yticklabelsize = 13

hlines!(ax, [0], xmax=0.95, color="#989898", linewidth=1.2)

scatter!(xs, ys, color="#f26c08", strokewidth=0.5)
hidespines!(ax)

text!("Annual Global Surface Temperature,\nRelative to Late 19th Century Average",
      position=Point(xs[1], 1.2), offset=(10, -20), textsize=15, background=:white, font="Noto Bold")

text!("HOTTER THAN THE\n1880-1899 AVERAGE", position=Point(xs[end], 0), offset=(0, 25), align=(:right, :center), textsize=12, font="Noto")
text!("COLDER", position=Point(xs[end], 0), offset=(0, -20), align=(:right, :center), textsize=12, font="Noto")

labelyears = [1904, 1944, 1998, 2014, 2015, 2016, 2017]
for year in labelyears
    text!(repr(year), position=Point(year, ys[findfirst(isequal(year), xs)]), align = (:right, :center),
          offset=(ifelse(year in [1904, 1998], -10, 40), 0), textsize=14, font="Noto " * ifelse(year==2016, "Bold", ""))
end

save("hotterthenthesun.png", f)
